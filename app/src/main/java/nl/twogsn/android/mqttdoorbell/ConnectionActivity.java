/*
 * Copyright 2020 Sjoerd van Leent
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.twogsn.android.mqttdoorbell;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.database.sqlite.SQLiteConstraintException;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import nl.twogsn.android.mqttdoorbell.data.AppDatabase;
import nl.twogsn.android.mqttdoorbell.model.Connection;

public class ConnectionActivity extends AppCompatActivity {

    /**
     * Determines whether the current instance is in "insert" or "update" mode.
     */
    private enum InsertOrUpdate {
        INSERT,
        UPDATE
    }

    private final ExecutorService executors;
    private InsertOrUpdate insertOrUpdate;

    /**
     * Creates an empty activity, with one executor thread
     */
    public ConnectionActivity() {
        super();
        this.executors = Executors.newFixedThreadPool(1);
    }

    /**
     * Creates the activity and (re-)initializes the screen elements.
     *
     * @param savedInstanceState The state (if available)
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connection);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        Button button = findViewById(R.id.save);
        button.setOnClickListener((v) -> this.saveConnection());

        if (savedInstanceState != null) {
            restoreState(savedInstanceState);
        } else {
            defineState();
        }

        if (insertOrUpdate == InsertOrUpdate.UPDATE) {
            findViewById(R.id.name).setEnabled(false);
        }
    }

    /**
     * Creates a fresh state. If the connection is set into the intent, it is interpreted
     * as an update, otherwise as an insert.
     */
    private void defineState() {
        Connection connection = (Connection) getIntent().getSerializableExtra("connection");
        if (connection != null) {
            restoreConnection(connection);
            insertOrUpdate = InsertOrUpdate.UPDATE;
        } else {
            insertOrUpdate = InsertOrUpdate.INSERT;
        }
    }

    /**
     * Restores the state from a saved instance after the screen orientation or something
     * likewise happened.
     *
     * @param savedInstanceState The data to restore
     */
    private void restoreState(Bundle savedInstanceState) {
        Connection connection = (Connection) savedInstanceState.getSerializable("connection");
        if (connection != null) {
            restoreConnection(connection);
        }
        if (savedInstanceState.getBoolean("insertOrUpdate")) {
            insertOrUpdate = InsertOrUpdate.INSERT;
        } else {
            insertOrUpdate = InsertOrUpdate.UPDATE;
        }
    }

    /**
     * Save the connection as realized into the database
     */
    private void saveConnection() {
        Connection connection = createConnection();
        if (verifyConnection(connection)) {
            executors.execute(() -> {
                switch (insertOrUpdate) {
                    // Attempts to add the connection into the database. There is a constraint on
                    // the name, so it is not possible to add a connection with the same name. Show
                    // an error if such an attempt has been made, and allow the user to correct it.
                    case INSERT:
                        try {
                            AppDatabase.insertConnection(connection);
                        } catch (SQLiteConstraintException ex) {
                            showCannotInsertConnectionSimilarNameError();
                            return;
                        }

                    case UPDATE:
                        AppDatabase.updateConnection(connection);
                }

                this.runOnUiThread(() -> {
                    setResult(RESULT_OK);
                    finish();
                });
            });
        }
    }

    private boolean verifyConnection(Connection connection) {
        if (!connection.createURI().isPresent()) {
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            String msg = getResources().getString(R.string.wrong_url);
            dialog.setMessage(msg);
            dialog.setTitle(connection.getName());
            dialog.setPositiveButton(R.string.ok, (d, w) -> d.dismiss());
            dialog.create().show();
            return false;
        }
        return true;
    }

    /**
     * A dialog is shown when it is not possible to store the date due to the name
     * field to be used twice.
     */
    private void showCannotInsertConnectionSimilarNameError() {
        runOnUiThread(() -> {
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setMessage(R.string.cannot_insert_connection_similar_name);
            dialog.setTitle(R.string.error);
            dialog.setPositiveButton(R.string.ok, (d, w) -> d.dismiss());
            dialog.create().show();
        });
    }

    /**
     * Restores the user interface elements using the existing connection data.
     * This is used when the data needs to be restored from a screen action, such
     * as a rotate, or when an update is to be performed instead of an insert.
     *
     * @param connection The connection to restore the data from
     */
    private void restoreConnection(Connection connection) {
        TextView nameView = findViewById(R.id.name);
        TextView urlView = findViewById(R.id.url);
        TextView topicView = findViewById(R.id.topic);
        nameView.setText(connection.getName());
        urlView.setText(connection.getUrl());
        topicView.setText(connection.getTopic());
    }

    /**
     * Gathers the connection from the user interface elements
     * @return A connection from the user interface elements
     */
    private Connection createConnection() {
        TextView nameView = findViewById(R.id.name);
        TextView urlView = findViewById(R.id.url);
        TextView topicView = findViewById(R.id.topic);
        CharSequence name = nameView.getText();
        CharSequence url = urlView.getText();
        CharSequence topic = topicView.getText();
        return new Connection(name.toString(), url.toString(), topic.toString());
    }

    /**
     * If the instance needs to be recreated (for example when rotating the screen), the
     * data is saved such that it can be easily recreated later with the contents still intact.
     * @param outState The state to write the data to
     */
    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putSerializable("connection", createConnection());
        outState.putBoolean("insertOrUpdate", insertOrUpdate == InsertOrUpdate.INSERT);
        super.onSaveInstanceState(outState);
    }

    /**
     * If the home button has been pressed or touched, set the result to cancel the request and
     * return to the calling activity.
     *
     * @param item The item being pressed
     * @return The result of the super class implementation of the same method
     */
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            setResult(RESULT_CANCELED);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
