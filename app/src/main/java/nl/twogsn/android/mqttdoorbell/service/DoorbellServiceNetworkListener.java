/*
 * Copyright 2020 Sjoerd van Leent
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.twogsn.android.mqttdoorbell.service;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.LinkProperties;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkRequest;
import android.util.Log;

import androidx.annotation.NonNull;

import java.util.ArrayList;

/**
 * This is an inline class which is used to react on whether a network became available or
 * was lost. This is used by the connectivity manager, which is launched by the
 * registerDoorbellServiceNetworkCallback() method.
 */
class DoorbellServiceNetworkListener extends ConnectivityManager.NetworkCallback {
    private static final String TAG = "DoorbellService/NL";
    private final DoorbellService doorbellService;
    private boolean registered = false;

    /**
     * This class is used to react on network changes
     * @param doorbellService The doorbell service which to notify whether a network has
     *                        become available or is lost.
     */
    public DoorbellServiceNetworkListener(DoorbellService doorbellService) {
        this.doorbellService = doorbellService;
    }

    /**
     * Reacts when the network has become available
     * @param network The network being available
     */
    @Override
    public void onAvailable(@NonNull Network network) {
        Log.i(TAG, "Network has become available");
        doorbellService.reloadConnections();
    }

    /**
     * Reacts when the link properties of a network has been changed
     * @param network The network being changed
     * @param linkProperties The new link properties
     */
    @Override
    public void onLinkPropertiesChanged(@NonNull Network network,
                                        @NonNull LinkProperties linkProperties) {
        Log.i(TAG, "Network's properties have been changed");
        doorbellService.reloadConnections();
    }

    /**
     * Reacts when the capabilities of a network have been changed
     * @param network The network being changed
     * @param networkCapabilities The new capabilities
     */
    @Override
    public void onCapabilitiesChanged(@NonNull Network network,
                                      @NonNull NetworkCapabilities networkCapabilities) {
        Log.i(TAG, "Network's properties have been changed");
        doorbellService.reloadConnections();
    }

    /**
     * Reacts when the blocked of a network has been changed
     * @param network The network being changed
     * @param blocked Whether the network is blocked or not
     */
    @Override
    public void onBlockedStatusChanged(@NonNull Network network, boolean blocked) {
        Log.i(TAG, "Network's properties have been changed");
        doorbellService.reloadConnections();
    }

    /**
     * Reacts when the network has been lost
     * @param network The network being lost
     */
    @Override
    public void onLost(@NonNull Network network) {
        Log.i(TAG, "Network has been lost");
        doorbellService.reloadConnections();
    }

    public void register() {
        if (this.registered) {
            throw new AssertionError(
                    "Already registered to the doorbell service's application context");
        }
        Log.i(TAG, "Registering to connectivity manager");
        ConnectivityManager connectivityManager = (ConnectivityManager)this.doorbellService
                .getApplicationContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        assert connectivityManager != null;
        NetworkRequest networkRequest = new NetworkRequest.Builder()
                .addCapability(NetworkCapabilities.NET_CAPABILITY_TRUSTED)
                .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
                .addTransportType(NetworkCapabilities.TRANSPORT_ETHERNET).build();
        connectivityManager.registerNetworkCallback(networkRequest, this);
        this.registered = true;
    }
}
