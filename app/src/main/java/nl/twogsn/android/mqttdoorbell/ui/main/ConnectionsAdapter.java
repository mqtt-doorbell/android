/*
 * Copyright 2020 Sjoerd van Leent
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.twogsn.android.mqttdoorbell.ui.main;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import nl.twogsn.android.mqttdoorbell.R;
import nl.twogsn.android.mqttdoorbell.model.Connection;
import nl.twogsn.android.mqttdoorbell.ui.interfaces.OnConnectionInteractionListener;

import java.util.ArrayList;
import java.util.List;

/**
 * This adapter manages the connections defined by the main activity. When the change or remove
 * button has been pressed, it sends them to the listener, which must be defined by the implementing
 * activity.
 */
public class ConnectionsAdapter extends RecyclerView.Adapter<ConnectionsAdapter.ViewHolder> {

    private List<Connection> connections = new ArrayList<>();
    private OnConnectionInteractionListener listener = null;

    public ConnectionsAdapter() {
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.simple_connection, parent, false);
        return new ViewHolder(view, this);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.setConnection(connections.get(position));
    }

    @Override
    public int getItemCount() {
        return connections.size();
    }

    public void setConnections(List<Connection> connections) {
        this.connections.clear();
        this.connections.addAll(connections);
        this.notifyDataSetChanged();
    }

    /**
     * Sets the listener which listens on the change and remove buttons being pressed
     * @param listener The listener which acts on change and remove actions
     */
    public void setListener(OnConnectionInteractionListener listener) {
        this.listener = listener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView item;
        private final TextView url;
        private final ConnectionsAdapter adapter;
        private final Button changeButton;
        private final Button removeButton;
        private Connection conn;

        public ViewHolder(View view, ConnectionsAdapter adapter) {
            super(view);
            this.adapter = adapter;
            this.item = view.findViewById(R.id.connection_item);
            this.url = view.findViewById(R.id.connection_url);
            this.changeButton = view.findViewById(R.id.change);
            this.removeButton = view.findViewById(R.id.remove);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + item.getText() + "'";
        }

        public void setConnection(Connection conn) {
            this.conn = conn;
            this.item.setText(conn.getName());
            this.url.setText(conn.getUrlAndTopic());
            this.removeButton.setOnClickListener(null);
            this.changeButton.setOnClickListener(null);
            if (listener != null) {
                this.removeButton.setOnClickListener((v) -> listener.onRemove(this.conn));
                this.changeButton.setOnClickListener((v) -> listener.onChange(this.conn));
            }
        }

    }
}
