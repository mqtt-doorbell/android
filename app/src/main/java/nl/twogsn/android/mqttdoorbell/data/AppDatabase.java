/*
 * Copyright 2020 Sjoerd van Leent
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.twogsn.android.mqttdoorbell.data;

import android.annotation.SuppressLint;
import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import java.util.List;

import nl.twogsn.android.mqttdoorbell.model.Connection;

/**
 * Implements the database using the Room SDK
 */
@Database(entities={Connection.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    /**
     * According to Stack overflow article:
     * https://stackoverflow.com/questions/2002288/static-way-to-get-context-in-android
     * This is the right way to do it
     */
    @SuppressLint("StaticFieldLeak")
    private static Context context = null;

    /**
     * Gets the database access object for connections
     * @return An instance of ConnectionDao
     */
    public abstract ConnectionDao connectionDao();

    /**
     * Sets the context for the app database to use
     * @param context The context to set (this needs to be done early in process)
     */
    public static void setContext (Context context) {
        AppDatabase.context = context;
    }

    /**
     * Returns an instance of the app database
     * @return An instance of the app database
     */
    public static AppDatabase getInstance() {
        if (context != null) {
            return Room.databaseBuilder(context, AppDatabase.class, "mqtt-doorbell").build();
        } else {
            return null;
        }
    }

    /**
     * Retrieves all connections using the connection DAO
     * @return All known connections
     */
    public static List<Connection> retrieveConnections() {
        ConnectionDao dao = getConnectionDao();
        return dao.getConnections();
    }

    /**
     * Removes a connection from the database
     * @param connection The connection to remove
     */
    public static void deleteConnection(Connection connection) {
        ConnectionDao dao = getConnectionDao();
        dao.deleteConnection(connection);
    }

    /**
     * Inserts a connection into the database
     * @param connection The connection to insert
     */
    public static void insertConnection(Connection connection) {
        ConnectionDao dao = getConnectionDao();
        dao.insertConnection(connection);
    }

    /**
     * Updates a connection in the database
     * @param connection The connection to update
     */
    public static void updateConnection(Connection connection) {
        ConnectionDao dao = getConnectionDao();
        dao.updateConnection(connection);
    }

    /**
     * Gets the connection DAO
     * @return The connection DAO
     */
    private static ConnectionDao getConnectionDao() {
        AppDatabase instance = getInstance();
        assert instance != null;
        return instance.connectionDao();
    }
}
