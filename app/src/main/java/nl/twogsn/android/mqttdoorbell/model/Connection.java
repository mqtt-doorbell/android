/*
 * Copyright 2020 Sjoerd van Leent
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.twogsn.android.mqttdoorbell.model;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Objects;
import java.util.Optional;

/**
 * This class represents the model of a connection. A connection typically has a name, URL and
 * topic to connect to. The name represents the name used for human readability. The URL and topic
 * are machine readable attributes.
 */
@Entity
public class Connection implements Serializable {

    private static final String TAG = "Connection";

    /**
     * The name of the connection
     */
    @PrimaryKey
    @NonNull
    private String name;

    /**
     * The URL of the MQTT broker to connect to
     */
    @ColumnInfo(name="url")
    @NonNull
    private String url;

    /**
     * The topic to connect to
     */
    @ColumnInfo(name="topic")
    @NonNull
    private String topic;

    /**
     * Creates a connection with the name, URL and topic set.
     * @param name The name to use
     * @param url The URL to use
     * @param topic The topic to use
     */
    public Connection(@NonNull String name, @NonNull String url, @NonNull String topic) {
        this.name = name;
        this.url = url;
        this.topic = topic;
    }

    /**
     * Returns the name of the connection
     * @return A string containing the name
     */
    @NonNull
    public String getName() {
        return name;
    }

    /**
     * Returns the URL of the connection
     * @return A string containing the URL
     */
    @NonNull
    public String getUrl() {
        return url;
    }

    /**
     * Returns the topic of the connection
     * @return A string containing the topic
     */
    @NonNull
    public String getTopic() {
        return topic;
    }

    /**
     * Returns a concatenated version of the URL and topic as one string
     * @return A string containing a concatenated URL and topic
     */
    public String getUrlAndTopic() {
        if (createURI().isPresent()) {
            URI proper = createURI().get();
            if (proper.getPort() == -1) {
                return proper.getScheme() + "://" + proper.getHost() + "/" + topic;
            } else {
                return proper.getScheme() + "://" + proper.getHost() + ":" + proper.getPort() + "/" + topic;
            }
        }
        return "";
    }

    /**
     * Returns the hash code for lookup and indexing
     * @return The hash code of the name, this is a primary key and therefore usable for the
     * hash code
     */
    @Override
    public int hashCode() {
        return name.hashCode();
    }

    /**
     * Attempts to match this instance with another instance
     * @param obj The other object to match
     * @return Whether the objects match
     */
    @Override
    public boolean equals(@Nullable Object obj) {
        return obj instanceof Connection && Objects.equals(((Connection) obj).name, name);
    }

    /**
     * Returns a Java URI object from the url, which must conform to the proper schema
     *
     * @return An optional URI, this should contain the url when the url is valid, otherwise
     * the url is invalid.
     */
    public Optional<URI> createURI() {
        try {
            URI uri = new URI(getUrl());
            if (uri.getScheme().equals("mqtt") &&
                    (uri.getPath() == null || uri.getPath().isEmpty())) {
                return Optional.of(uri);
            }
        } catch (URISyntaxException e) {
            // pass
        }
        Log.e(TAG, "Connection URL incompatible");
        return Optional.empty();
    }
}
