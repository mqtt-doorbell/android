/*
 * Copyright 2020 Sjoerd van Leent
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.twogsn.android.mqttdoorbell.service;

import android.os.Binder;

/**
 * The doorbell binder exposes the interface of the doorbell service to other context's within
 * the app.
 */
public class DoorbellBinder extends Binder implements IDoorbellBinder {

    private final DoorbellService service;


    /**
     * Constructs a doorbell binder, with the doorbell service embedded
     * @param service The doorbell service to bind to
     */
    public DoorbellBinder(DoorbellService service) {
        this.service = service;
    }

    /**
     * Requests the service to reload the connections from the database, and to subscribe or
     * unsubscribe accordingly.
     */
    @Override
    public void reloadConnections() {
        this.service.reloadConnections();
    }

}
