/*
 * Copyright 2020 Sjoerd van Leent
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.twogsn.android.mqttdoorbell.ui.main;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import nl.twogsn.android.mqttdoorbell.R;

/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class SectionsPagerAdapter extends FragmentPagerAdapter {

    @StringRes
    private static final int[] TAB_TITLES = new int[] {
            R.string.tab_connections,
            R.string.tab_about};

    private final Context context;

    /**
     * Constructs an instance of the adapter, based on a context and fragment manager.
     * @param context The context, typically an activity
     * @param fm The fragment manager
     */
    public SectionsPagerAdapter(Context context, FragmentManager fm) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        this.context = context;
    }

    /**
     * Returns the proper fragment based on the position. This is a creator method, and
     * instantiates a fragment every time called.
     *
     * @param position The position of the fragment to use
     * @return A fragment based on the position
     */
    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position)
        {
            case 0:
                return new ConnectionsFragment();
            default:
                return new AboutFragment();
        }
    }

    /**
     * Gets the title of a particular fragment based on the position.
     * @param position The position of the fragment whereof a title must be fetched
     * @return The title for a particular fragment
     */
    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return this.context.getResources().getString(TAB_TITLES[position]);
    }

    /**
     * Returns the count of tabs within the pager.
     * @return The count of tabs
     */
    @Override
    public int getCount() {
        // Show 2 total pages.
        return TAB_TITLES.length;
    }

}