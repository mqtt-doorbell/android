/*
 * Copyright 2020 Sjoerd van Leent
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.twogsn.android.mqttdoorbell.service;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;

import java.util.Optional;

/**
 * The doorbell service connection is used in the main activity, to connect to the binder
 * which exposes the doorbell service interface.
 */
public class DoorbellServiceConnection implements ServiceConnection {

    private DoorbellBinder binder;

    /**
     * Connects the service connection to the binder
     *
     * @param name The component name to which the connection has been bound
     * @param service The service binder to which the connection has been bound
     */
    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        this.binder = (DoorbellBinder) service;

    }

    /**
     * Disconnects the service connection to the binder
     *
     * @param name The component name to which the connection was bound
     */
    @Override
    public void onServiceDisconnected(ComponentName name) {
        binder = null;
    }


    /**
     * Gets the doorbell binder interface to the binder of the associated doorbell service.
     *
     * @return The interface, if available
     */
    public Optional<IDoorbellBinder> getBinder() {
        if (this.binder == null) {
            return Optional.empty();
        } else {
            return Optional.of(binder);
        }
    }

    /**
     * Check if the doorbell binder interface is bound to the doorbell service.
     *
     * @return True if the doorbell binder is bound to the doorbell service, false otherwise.
     */
    public boolean isBound() {
        return binder != null;
    }
}
