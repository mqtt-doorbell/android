/*
 * Copyright 2020 Sjoerd van Leent
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.twogsn.android.mqttdoorbell;

import android.content.Intent;
import android.os.Bundle;
import android.text.BidiFormatter;

import com.google.android.material.tabs.TabLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import nl.twogsn.android.mqttdoorbell.data.AppDatabase;
import nl.twogsn.android.mqttdoorbell.model.Connection;
import nl.twogsn.android.mqttdoorbell.service.DoorbellService;
import nl.twogsn.android.mqttdoorbell.service.DoorbellServiceConnector;
import nl.twogsn.android.mqttdoorbell.service.IDoorbellBinder;
import nl.twogsn.android.mqttdoorbell.ui.interfaces.OnConnectionInteractionListener;
import nl.twogsn.android.mqttdoorbell.ui.main.ConnectionsAdapter;
import nl.twogsn.android.mqttdoorbell.ui.main.OnSettingsInteractionListener;
import nl.twogsn.android.mqttdoorbell.ui.main.SectionsPagerAdapter;

public class MainActivity extends AppCompatActivity implements
        OnConnectionInteractionListener,
        OnSettingsInteractionListener {

    private final ExecutorService executors;
    private final DoorbellServiceConnector doorbellServiceConnector;

    /**
     * Creates the main activity, which allows a connection to the doorbell service, and a number
     * of worker executors to write changes to the database.
     */
    public MainActivity() {
        super();
        this.executors = Executors.newFixedThreadPool(2);
        this.doorbellServiceConnector = new DoorbellServiceConnector(this);
    }

    /**
     * This method is called when the activity is created. It will create the necessary
     * UI content for main activity, containing the list of doorbells and a settings area.
     *
     * @param savedInstanceState Not used, there is no state to be saved, it will if necessary
     *                           be restored from the room.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DoorbellService.startDoorbellService(getApplicationContext());
        setContentView(R.layout.activity_main);
        SectionsPagerAdapter sectionsPagerAdapter =
                new SectionsPagerAdapter(this, getSupportFragmentManager());
        ViewPager viewPager = findViewById(R.id.view_pager);
        viewPager.setAdapter(sectionsPagerAdapter);
        TabLayout tabs = findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);
    }

    /**
     * This method ensures that the service connection is connected to the doorbell service as
     * instantiated either by the onCreate() method, or the boot process.
     */
    @Override
    protected void onStart() {
        super.onStart();
        doorbellServiceConnector.connectToDoorbellService();
    }

    /**
     * This method ensures that the service connection is disconnected to the doorbell service when
     * the activity is stopped.
     */
    @Override
    protected void onStop() {
        super.onStop();
        doorbellServiceConnector.disconnectFromDoorbellService();
    }

    /**
     * When the remove button is pressed, a dialog is presented requesting the user to make
     * sure it is ok to remove the connection. If so, the connection is removed from the database,
     * otherwise the operation is cancelled.
     *
     * @param connection The connection for which the remove button has been pressed.
     */
    @Override
    public void onRemove(Connection connection) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        String msg = getResources().getString(R.string.remove_connection_message);
        dialog.setMessage(
                String.format(msg, BidiFormatter.getInstance().unicodeWrap(connection.getName())));
        dialog.setTitle(R.string.remove_connection_title);
        dialog.setPositiveButton(R.string.ok, (d, w) -> {
            deleteConnection(connection);
            d.dismiss();
        });
        dialog.setNegativeButton(R.string.cancel, (d, w) -> d.dismiss());
        dialog.setCancelable(true);
        dialog.create().show();
    }

    /**
     * When the change button is pressed, the main activity switches to the connection activity,
     * with the presented connection. The connection is then changed, using the new parameters.
     *
     * @param connection The connection for which the change button has been pressed.
     */
    @Override
    public void onChange(Connection connection) {
        Intent intent = new Intent(this, ConnectionActivity.class);
        intent.putExtra("connection", connection);
        startActivityForResult(intent, 1);
    }

    /**
     * When the Add button is pressed, the main activity switches to the connection activity,
     * to allow adding a new connection.
     */
    @Override
    public void onAdd() {
        startActivityForResult(new Intent(this, ConnectionActivity.class), 1);
    }

    /**
     * When the Save button has been pressed in the Intent of the ConnectionActivity created
     * by the Add button, the result will be RESULT_OK, and the connection has been saved. The
     * connections are to be reloaded and refreshed into the connections list.
     * @param requestCode The request code started with the initial intent (the Add button uses: 1)
     * @param resultCode The result code, should be RESULT_OK
     * @param data The data, absent for the Add button
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == 1 && resultCode == RESULT_OK)
        {
            loadConnections();
            doorbellServiceConnector.getBinder().ifPresent(IDoorbellBinder::reloadConnections);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * Loads the connections from the application database and shows them into the connections
     * list, part of the ConnectionFragment.
     */
    @Override
    public void loadConnections() {
        executors.execute(() -> {
            List<Connection> connections = AppDatabase.retrieveConnections();
            this.runOnUiThread(
                    () -> this.putConnectionsIntoConnectionsList(connections)
            );
        });
    }

    /**
     * Removes a connection from the database, on a separate thread, and reloads the connections.
     * @param connection The connection to remove
     */
    public void deleteConnection(Connection connection) {
        executors.execute(() -> {
            AppDatabase.deleteConnection(connection);
            loadConnections();
        });
    }

    /**
     * Puts the connections into the list, by sending them to the recycler view's adapter.
     * @param connections The connections to put into the connections list.
     */
    private void putConnectionsIntoConnectionsList(List<Connection> connections) {
        RecyclerView view = findViewById(R.id.list);
        ConnectionsAdapter adapter = (ConnectionsAdapter) view.getAdapter();
        assert adapter != null;
        adapter.setConnections(connections);
    }
}