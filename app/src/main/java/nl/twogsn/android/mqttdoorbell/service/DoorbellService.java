/*
 * Copyright 2020 Sjoerd van Leent
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.twogsn.android.mqttdoorbell.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.IBinder;
import android.os.PowerManager;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.annotation.StringRes;
import androidx.core.app.NotificationCompat;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

import nl.twogsn.android.mqttdoorbell.MainActivity;
import nl.twogsn.android.mqttdoorbell.R;
import nl.twogsn.android.mqttdoorbell.data.AppDatabase;
import nl.twogsn.android.mqttdoorbell.model.Connection;
import nl.twogsn.android.mqttdoorbell.mqtt.MqttHandler;

/**
 * The doorbell service is basically the guts of the app. It creates the necessary connections
 * to the MQTT Broker(s) based on the configurations configured in the database.
 */
public class DoorbellService extends Service {

    private static final String CHANNEL_ID = "doorbell-service";
    private static final int NOTIFICATION_ID = 1;
    private static final String TAG = "DoorbellService";

    private final IDoorbellBinder binder;
    private final ExecutorService executors;

    private final HashMap<Connection, MqttHandler> managedConnections = new HashMap<>();
    private final HashMap<Connection, Integer> notificationIdentifiers = new HashMap<>();
    private MediaPlayer mediaPlayer;


    public DoorbellService() {
        this.executors = Executors.newFixedThreadPool(1);
        binder = new DoorbellBinder(this);
    }

    /**
     * Starts the doorbell service using some application context. If the android version
     * is >= Oreo, then the doorbell service will start as a foreground service and will
     * create a notification channel, such that the notification is properly displayed
     *
     * @param context An application context
     */
    public static void startDoorbellService(Context context) {
        Intent intent = new Intent(context.getApplicationContext(), DoorbellService.class);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChannel(context);
            context.startForegroundService(intent);
        } else {
            context.startService(intent);
        }
    }

    /**
     * Creates the notification channel, only if the version is >= Oreo, otherwise this
     * method does not exist.
     *
     * @param context An application context
     */
    @RequiresApi(api = Build.VERSION_CODES.O)
    private static void createNotificationChannel(Context context) {
        CharSequence name = context.getString(R.string.channel_name);
        String description = context.getString(R.string.channel_description);
        NotificationChannel channel = new NotificationChannel(
                CHANNEL_ID, name, NotificationManager.IMPORTANCE_NONE);
        channel.setDescription(description);
        channel.setShowBadge(false);
        // Register the channel with the system; you can't change the importance
        // or other notification behaviors after this
        NotificationManager notificationManager = context
                .getSystemService(NotificationManager.class);
        assert notificationManager != null;
        notificationManager.createNotificationChannel(channel);
    }

    /**
     * Reloads the connections from the database.
     */
    public void reloadConnections() {
        notify(R.string.updating_connections);
        Log.i(TAG, "Loading the connections from the database, if network is available");
        resetConnections();
    }

    private void resetConnections() {
        executors.execute(() -> {
            synchronized (managedConnections) {
                List<Connection> connections = AppDatabase.retrieveConnections();

                removeUnknownConnections(connections);

                for (Connection connection : connections) {
                    if (isExistingConnection(connection)) {
                        manageExistingConnection(connection);
                    } else {
                        manageNewConnection(connection);
                    }
                }

                resetNotificationChannelIdentifiers();
            }
        });
    }

    private boolean isExistingConnection(Connection connection) {
        return this.managedConnections.containsKey(connection);
    }

    private void manageExistingConnection(Connection connection) {
        this.managedConnections.get(connection).resetDelay();
    }

    private void manageNewConnection(Connection connection) {
        Optional<URI> uri = connection.createURI();

        uri.ifPresent((t) -> {
            MqttHandler cm = new MqttHandler(connection, t, this);
            cm.connectToConnection();
            this.managedConnections.put(connection, cm);
        });
        if (!uri.isPresent()) {
            this.notify(connection, R.string.wrong_url);
        }
    }

    private void removeUnknownConnections(List<Connection> connections) {
        Set<Connection> keySet = this.managedConnections.keySet();
        for (Connection managedConnection : keySet) {
            if (!connections.contains(managedConnection)) {
                MqttHandler handler = this.managedConnections.remove(
                        connections.get(connections.indexOf(managedConnection)));
                if (handler != null) {
                    handler.disconnectConnection();
                }
            }
        }
    }

    private void resetNotificationChannelIdentifiers() {
        AtomicInteger i = new AtomicInteger(2);
        this.managedConnections.keySet().forEach(c ->
        {
            notificationIdentifiers.put(c, i.intValue());
            i.getAndIncrement();
        });
    }

    @Override
    public void onCreate() {
        super.onCreate();
        reloadConnections();
    }

    /**
     * This method is called when the service is being started. The service registers itself
     * to doorbell service network listener, which sends whether the network has become available
     * or has been lost.
     *
     * @param intent The intent used to launch the service
     * @param flags The flags passed
     * @param startId The start identifier
     * @return The result value of the call to the super class
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        MediaPlayer mediaPlayer = MediaPlayer.create(this, R.raw.door_chime_sound);
        mediaPlayer.setWakeMode(this.getApplicationContext(), PowerManager.PARTIAL_WAKE_LOCK);
        this.mediaPlayer = mediaPlayer;
        new DoorbellServiceNetworkListener(this).register();
        return super.onStartCommand(intent, flags, startId);
    }


    /**
     * Shows a string as a notification about the current activity of the MQTT Doorbell
     *
     * @param stringId The text to show
     */
    public void notify(@StringRes int stringId) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);
        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setOngoing(true)
                .setSmallIcon(R.drawable.ic_notification_icon)
                .setContentTitle(getResources().getString(stringId))
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentIntent(pendingIntent).build();
        startForeground(NOTIFICATION_ID, notification);
    }

    /**
     * Shows a string as a notification about the current activity of the MQTT Doorbell
     *
     * @param connection The connection for which to show a notification
     * @param stringId The text to show
     */
    public void notify(Connection connection, @StringRes int stringId) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);
        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_notification_icon)
                .setContentTitle(connection.getName())
                .setContentText(getResources().getString(stringId))
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentIntent(pendingIntent).build();
        if (notificationIdentifiers.containsKey(connection) &&
            notificationIdentifiers.get(connection) != null) {
            startForeground(notificationIdentifiers.get(connection), notification);
        }
    }


    /**
     * Binds the binder to the service. This will be used by other parts of the application
     * to retrieve the necessary service calls.
     *
     * @param intent The intent which is send for binding (it is ignored)
     * @return The binder of the IDoorbellBinder type
     */
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    /**
     * This is to be executed when the doorbell needs to be sound
     * @param connection The connection for which the doorbell needs to be sound
     */
    public void soundDoorbell(Connection connection) {
        this.mediaPlayer.start();
        notify(connection, R.string.doorbell_pressed);
    }
}
