/*
 * Copyright 2020 Sjoerd van Leent
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.twogsn.android.mqttdoorbell.mqtt;

import android.util.Log;

import androidx.annotation.StringRes;

import com.hivemq.client.mqtt.MqttClient;
import com.hivemq.client.mqtt.lifecycle.MqttClientDisconnectedContext;
import com.hivemq.client.mqtt.mqtt5.Mqtt5AsyncClient;

import java.net.URI;
import java.util.Locale;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import nl.twogsn.android.mqttdoorbell.R;
import nl.twogsn.android.mqttdoorbell.model.Connection;
import nl.twogsn.android.mqttdoorbell.service.DoorbellService;

/**
 * Implements the handler, which maintains the connection state to the MQTT broker for a
 * given connection.
 */
public class MqttHandler {

    private static final String TAG = "ConnectionManager";
    private final URI uri;
    private final DoorbellService origin;
    private final ScheduledExecutorService executors;
    private final Connection connection;
    private boolean shouldConnect;
    private int delay = 0;
    private final Mqtt5AsyncClient client;
    private boolean connected;

    /**
     * Constructs the connection manager with a URI (containing the URL of the connection),
     * a topic and the origin service. The URI given here is expected to be clean (valid).
     * @param uri The uri to connect to
     * @param connection The connection to connect to, containing the name and topic
     * @param origin The origin, this can be used to send notifications
     */
    public MqttHandler(Connection connection, URI uri, DoorbellService origin) {
        this.connection = connection;
        this.uri = uri;
        this.origin = origin;
        this.executors = Executors.newSingleThreadScheduledExecutor();
        this.shouldConnect = false;
        this.connected = false;

        this.client = MqttClient.builder()
                .useMqttVersion5()
                .identifier("MQTTDoorbell-" + UUID.randomUUID().toString())
                .serverHost(this.uri.getHost())
                .serverPort(this.uri.getPort() == -1 ? 1883 : this.uri.getPort())
                .addDisconnectedListener(this::onDisconnected)
                .buildAsync();
    }


    private String getTopic() {
        return this.connection.getTopic();
    }

    /**
     * Subscribes and connects the connection to the MQTT broker.
     */
    public void connectToConnection() {
        synchronized (client) {
            // If shouldConnect is set to true here, there is already an ongoing attempt to
            // connection.
            if (shouldConnect) { return; }
            shouldConnect = true;
            attemptConnection();
        }
    }

    /**
     * Attempts to create the connection either because connectToConnection() instructs to do
     * so, a retry timeout has expired or a disconnect occurred.
     */
    private void attemptConnection() {
        if (!connected) {
            Log.i(TAG, String.format(Locale.ENGLISH,
                    "Attempting to connect to %s/%s after delay %d",
                    this.uri, getTopic(), this.delay));
            client.connect().whenComplete((connAck, throwable) -> {
                if (throwable != null) {
                    configureRetryAfterError(R.string.cannot_connect);
                } else {
                    onConnected();
                }
            });
        }
    }

    /**
     * This method is triggered when connection is successful. It starts subscription
     */
    private void onConnected() {
        Log.i(TAG, String.format("Connected to %s/%s", this.uri, getTopic()));
        Log.i(TAG, String.format("Attempting to subscribe to %s/%s", this.uri, getTopic()));
        client.subscribeWith()
                .topicFilter(getTopic())
                .callback((publish) -> origin.soundDoorbell(this.connection))
                .send()
                .whenComplete((subAck, subThrowable) -> {
                    if (subThrowable != null) {
                        try {
                            client.disconnect().get();
                        } catch (ExecutionException | InterruptedException e) {
                            // pass
                        }
                        configureRetryAfterError(R.string.cannot_subscribe);
                    } else {
                        onSubscribed();
                    }
                });
    }

    /**
     * This method is triggered when subscription is successful. It resets delays and notifies
     * the user of successful connection.
     */
    private void onSubscribed() {
        Log.i(TAG, String.format("Subscribed to %s/%s", this.uri, getTopic()));
        synchronized (client) {
            if (!shouldConnect) {
                forceUnsubscribeAndDisconnect();
            } else {
                this.connected = true;
                Log.i(TAG, String.format("Connected/subscribed to  %s/%s", this.uri, getTopic()));
                origin.notify(this.connection, R.string.connected);
                resetDelay();
            }
        }
    }

    /**
     * Forces an unsubscribe and disconnect to happen, this is performed on network loss or
     * severe change
     */
    private void forceUnsubscribeAndDisconnect() {
        Log.i(TAG, String.format("Should unsubscribe from %s/%s", this.uri, getTopic()));
        try {
            client.unsubscribeWith().topicFilter(getTopic()).send().get();
        } catch (ExecutionException | InterruptedException e) {
            Log.e(TAG, "Can not unsubscribe properly from " + getTopic());
        }
        forceDisconnect();
    }

    /**
     * Forces a disconnect to happen, this is performed on network loss or severe change
     */
    private void forceDisconnect() {
        Log.i(TAG, String.format("Should disconnect from %s/%s", this.uri, getTopic()));
        try {
            client.disconnect().get();
        } catch (ExecutionException | InterruptedException e) {
            Log.e(TAG, "Can not disconnect properly from " + getTopic());
        }
    }

    /**
     * Unsubscribes (if subscribed) and disconnect (if connected) the connection from the MQTT
     * broker.
     */
    public void disconnectConnection() {
        synchronized (client) {
            shouldConnect = false;
            resetDelay();
            origin.notify(this.connection, R.string.cannot_connect);
            forceUnsubscribeAndDisconnect();
        }
    }

    /**
     * This method is called to reset the delay, this happens after a successful connection
     * and subscription has been established to the client.
     */
    public void resetDelay() {
        this.delay = 0;
        attemptConnection();
    }

    /**
     * Configures the retry after a connection or subscription failed. It increases a delay
     * on linear basis, as not to draw too much CPU effort.
     * @param stringId The string identification of the error situation to show.
     */
    private void configureRetryAfterError(@StringRes int stringId) {
        if (this.delay == 0) {
            origin.notify(this.connection, stringId);
        }
        int oldDelay = this.delay;
        // Increment the delay every time a new cycle is attempted, however,
        // limit the delay to 1 minute.
        if (this.delay < 60) {
            this.delay += 5;
        }
        // Try again after delay amount of seconds

        executors.schedule(new Runnable() {
            @Override
            public void run() {
                attemptConnection();
            }
        }, oldDelay, TimeUnit.SECONDS);
    }


    /**
     * Somehow, the client got disconnected, attempting to connect again. This can occur for
     * example when in a Mesh Wifi network, or a broker losing connection temporarily.
     * @param context The context within which the disconnection occurred
     */
    private void onDisconnected(MqttClientDisconnectedContext context) {
        this.connected = false;
        configureRetryAfterError(R.string.disconnected);
    }
}
