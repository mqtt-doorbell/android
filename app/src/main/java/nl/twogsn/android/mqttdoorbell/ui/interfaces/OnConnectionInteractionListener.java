/*
 * Copyright 2020 Sjoerd van Leent
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.twogsn.android.mqttdoorbell.ui.interfaces;

import java.util.List;

import nl.twogsn.android.mqttdoorbell.model.Connection;

/**
 * This interface must be implemented by activities that contain this
 * fragment to allow an interaction in this fragment to be communicated
 * to the activity and potentially other fragments contained in that
 * activity.
 */
public interface OnConnectionInteractionListener {

    /**
     * This method is triggered when the remove button has been pressed for a single Connection
     *
     * @param connection The connection for which the remove button has been pressed.
     */
    void onRemove(Connection connection);


    /**
     * This method is triggered when the change button has been pressed for a single Connection
     * Fragment.
     * @param connection The connection for which the change button has been pressed.
     */
    void onChange(Connection connection);

    /**
     * This method is triggered when the add button is pressed.
     */
    void onAdd();

    /**
     * Load the saved connections.
     */
    void loadConnections();

}
