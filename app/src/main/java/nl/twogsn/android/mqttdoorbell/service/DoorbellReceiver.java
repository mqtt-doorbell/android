/*
 * Copyright 2020 Sjoerd van Leent
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.twogsn.android.mqttdoorbell.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * This receiver registers a boot action, which launches the doorbell service (as frontend-service)
 * on the device after a reboot or startup.
 */
public class DoorbellReceiver extends BroadcastReceiver {
    private static final String TAG = "DoorbellReceiver";

    /**
     * Triggered when an action has been completed, in this case the ACTION_BOOT_COMPLETED
     * @param context The context of the receiver
     * @param intent The intent received
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        // If the intent results into an action boot completed, the doorbell service
        // will be started.
        if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
            Log.i(TAG, "Starting up Doorbell Service");
            DoorbellService.startDoorbellService(context);
        }
    }
}
