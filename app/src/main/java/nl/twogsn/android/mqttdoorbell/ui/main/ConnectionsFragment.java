/*
 * Copyright 2020 Sjoerd van Leent
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.twogsn.android.mqttdoorbell.ui.main;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import nl.twogsn.android.mqttdoorbell.R;
import nl.twogsn.android.mqttdoorbell.ui.interfaces.OnConnectionInteractionListener;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnConnectionInteractionListener}
 * interface.
 */
public class ConnectionsFragment extends Fragment {

    private ConnectionsAdapter adapter = new ConnectionsAdapter();
    private OnConnectionInteractionListener onConnectionInteractionListener;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ConnectionsFragment() {

    }

    /**
     * This method is called when the fragment is being attached properly to the activity
     * @param context The activity (context) the fragment is attached to
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnConnectionInteractionListener) {
            onConnectionInteractionListener = (OnConnectionInteractionListener)context;
            adapter.setListener(onConnectionInteractionListener);
        } else {
            throw new RuntimeException(
                    context.toString() + " must implement ConnectionEventListener");
        }
    }

    /**
     * This method is called when the view is to be created. It creates the internal recycler view
     * and sets the adapter on the recycler view, it connects the FAB to the listener, such that
     * it is able to add connections.
     *
     * @param inflater Inflates the fragment connection list into the container
     * @param container The container to put the fragment connection list into
     * @param savedInstanceState The instance state, this is not used
     * @return The view being created
     */
    @Override
    public View onCreateView(
            LayoutInflater inflater,
            ViewGroup container,
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_connections_list, container, false);
        RecyclerView recyclerView = view.findViewById(R.id.list);
        recyclerView.setAdapter(adapter);

        FloatingActionButton addButton = view.findViewById(R.id.add);

        if (this.onConnectionInteractionListener != null) {
            addButton.setOnClickListener((v) -> this.onConnectionInteractionListener.onAdd());
        }
        return view;

    }

    /**
     * This method is called after the view has been created, it notifies the interaction
     * listener about the need to load the connections.
     *
     * @param view The view - not used
     * @param savedInstanceState The saved instance - not used
     */
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        onConnectionInteractionListener.loadConnections();
    }

}
