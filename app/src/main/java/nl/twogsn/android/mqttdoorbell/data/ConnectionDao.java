/*
 * Copyright 2020 Sjoerd van Leent
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.twogsn.android.mqttdoorbell.data;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import nl.twogsn.android.mqttdoorbell.model.Connection;

/**
 * The interface of the connection database access object, which is instantiated by
 * the Android Room SDK using the App database.
 */
@Dao
public interface ConnectionDao {

    /**
     * Gets all the connections
     * @return The connections
     */
    @Query(" SELECT * FROM connection")
    List<Connection> getConnections();

    /**
     * Inserts a connection
     * @param connection the connection to insert
     */
    @Insert
    void insertConnection(Connection connection);

    /**
     * Deletes a connection
     * @param connection the connection to delete
     */
    @Delete
    void deleteConnection(Connection connection);

    /**
     * Updates a connection
     * @param connection the connection to update
     */
    @Update
    void updateConnection(Connection connection);
}
